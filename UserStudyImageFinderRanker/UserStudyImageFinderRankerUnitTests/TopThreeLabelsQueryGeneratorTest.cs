﻿using System.Collections.Generic;
using UserStudyImageFinderRanker;
using Xunit;

namespace UserStudyImageFinderRankerUnitTests
{
    public class TopThreeLabelsQueryGeneratorTest
    {
        private Dictionary<string, double> GetLabelsDictionary()
        {
            return new Dictionary<string, double>()
            {
                { "Black", 0.9849877953529358 },
                { "Darkness", 0.9739475250244141},
                { "White", 0.9654507637023926},
                { "Sky", 0.9336194396018982 },
                { "Light", 0.916901707649231 },
                { "Red", 0.9153681993484497 },
                { "Text", 0.9121394753456116 },
                { "Brown", 0.8869289755821228},
                { "Font", 0.8557851910591125 },
                { "Atmosphere", 0.8249121904373169 },
                { "Midnight", 0.7795843482017517 },
                { "Monochrome", 0.7715301513671875 }
            };
        }

        private Dictionary<string, double> GetLabelsDictionaryTwoItems()
        {
            return new Dictionary<string, double>()
            {
                { "Brown", 0.8869289755821228},
                { "Font", 0.8557851910591125 }
            };
        }

        [Fact]
        public void CorrectOutputTest()
        {
            var labelsMap = GetLabelsDictionary();

            var querySelector = new TopThreeLabelsQueryGenerator();

            var queries = querySelector.GenerateQueryFromLabels(new ImageLabels("someId", labelsMap));

            Assert.True(queries.Count == 3);
            Assert.True(queries[0] == "Black");
            Assert.True(queries[1] == "Darkness");
            Assert.True(queries[2] == "White");
        }

        [Fact]
        public void CorrectOutputTestTwoLabels()
        {
            var labelsMap = GetLabelsDictionaryTwoItems();

            var querySelector = new TopThreeLabelsQueryGenerator();

            var queries = querySelector.GenerateQueryFromLabels(new ImageLabels("someId", labelsMap));

            Assert.True(queries.Count == 2);
            Assert.True(queries[0] == "Brown");
            Assert.True(queries[1] == "Font");
        }
    }
}
