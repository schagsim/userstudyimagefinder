﻿using Xunit;

using UserStudyImageFinderRanker;
using System.Collections.Generic;

namespace UserStudyImageFinderRankerUnitTests
{
    public class RowsLabelSorterTest
    {
        private Dictionary<string, double> GenerateSorterInput()
        {
            return new Dictionary<string, double>()
            {
                { "Id1", 1.9 },
                { "Id2", 0.8 },
                { "Id3", 2.1 },
                { "Id4", 0.7 },
                { "Id5", 1.7 },
                { "Id6", 1.9 },
                { "Id7", 0.5 },
                { "Id8", 3.1 },
                { "Id9", 1.65 },
                { "Id10", 1.2 },
                { "Id11", 1.25 },
                { "Id12", 1.35 },
                { "Id13", 3.25 },
                { "Id14", 0.54 },
                { "Id15", 0.78 },
                { "Id16", 1.48 },
                { "Id17", 2.35 },
                { "Id18", 2.78 },
                { "Id19", 3.05 },
                { "Id20", 0.97 },
                { "Id21", 1.73 },
                { "Id22", 1.1 },
                { "Id23", 1.95 },
                { "Id24", 2.7 },
                { "Id25", 2.75 },
            };
        }


        [Fact]
        public void CorrectInput1()
        {
            var labelSorter = new RowsLabelSorter(GenerateSorterInput(), 5);

            var outputMatrix = labelSorter.Sort();

            Assert.True(outputMatrix.Length == 25);

            Assert.True(outputMatrix[0, 0] == "Id13");
            Assert.True(outputMatrix[0, 1] == "Id8");
            Assert.True(outputMatrix[0, 2] == "Id19");
            Assert.True(outputMatrix[0, 3] == "Id18");
            Assert.True(outputMatrix[0, 4] == "Id25");

            Assert.True(outputMatrix[1, 0] == "Id24");
            Assert.True(outputMatrix[1, 1] == "Id17");
            Assert.True(outputMatrix[1, 2] == "Id3");
            Assert.True(outputMatrix[1, 3] == "Id23");
            Assert.True(outputMatrix[1, 4] == "Id1");

            Assert.True(outputMatrix[2, 0] == "Id6");
            Assert.True(outputMatrix[2, 1] == "Id21");
            Assert.True(outputMatrix[2, 2] == "Id5");
            Assert.True(outputMatrix[2, 3] == "Id9");
            Assert.True(outputMatrix[2, 4] == "Id16");

            Assert.True(outputMatrix[3, 0] == "Id12");
            Assert.True(outputMatrix[3, 1] == "Id11");
            Assert.True(outputMatrix[3, 2] == "Id10");
            Assert.True(outputMatrix[3, 3] == "Id22");
            Assert.True(outputMatrix[3, 4] == "Id20");

            Assert.True(outputMatrix[4, 0] == "Id2");
            Assert.True(outputMatrix[4, 1] == "Id15");
            Assert.True(outputMatrix[4, 2] == "Id4");
            Assert.True(outputMatrix[4, 3] == "Id14");
            Assert.True(outputMatrix[4, 4] == "Id7");
        }

        [Fact]
        public void CorrectInput2()
        {
            var labelSorter = new RowsLabelSorter(GenerateSorterInput(), 6);

            var outputMatrix = labelSorter.Sort();

            Assert.True(outputMatrix.Length == 30);

            Assert.True(outputMatrix[0, 0] == "Id13");
            Assert.True(outputMatrix[0, 1] == "Id8");
            Assert.True(outputMatrix[0, 2] == "Id19");
            Assert.True(outputMatrix[0, 3] == "Id18");
            Assert.True(outputMatrix[0, 4] == "Id25");
            Assert.True(outputMatrix[0, 5] == "Id24");

            Assert.True(outputMatrix[1, 0] == "Id17");
            Assert.True(outputMatrix[1, 1] == "Id3");
            Assert.True(outputMatrix[1, 2] == "Id23");
            Assert.True(outputMatrix[1, 3] == "Id1");
            Assert.True(outputMatrix[1, 4] == "Id6");
            Assert.True(outputMatrix[1, 5] == "Id21");

            Assert.True(outputMatrix[2, 0] == "Id5");
            Assert.True(outputMatrix[2, 1] == "Id9");
            Assert.True(outputMatrix[2, 2] == "Id16");
            Assert.True(outputMatrix[2, 3] == "Id12");
            Assert.True(outputMatrix[2, 4] == "Id11");
            Assert.True(outputMatrix[2, 5] == "Id10");

            Assert.True(outputMatrix[3, 0] == "Id22");
            Assert.True(outputMatrix[3, 1] == "Id20");
            Assert.True(outputMatrix[3, 2] == "Id2");
            Assert.True(outputMatrix[3, 3] == "Id15");
            Assert.True(outputMatrix[3, 4] == "Id4");
            Assert.True(outputMatrix[3, 5] == "Id14");

            Assert.True(outputMatrix[4, 0] == "Id7");
            Assert.True(outputMatrix[4, 1] == "");
            Assert.True(outputMatrix[4, 2] == "");
            Assert.True(outputMatrix[4, 3] == "");
            Assert.True(outputMatrix[4, 4] == "");
            Assert.True(outputMatrix[4, 5] == "");
        }
    }
}
