﻿using UserStudyImageFinderRanker;
using Xunit;

namespace UserStudyImageFinderRankerUnitTests
{
    public class LabelParserTest
    {
        private const string testFilesFolder = @".\LabelFiles";

        [Fact]
        public void CorrectInput()
        {
            var output = LabelParser.ReadAndParseImageLabels(testFilesFolder);

            Assert.True(output.Count == 1);
            Assert.True(output[0].ImageId == "v00044_s00028(f017090-f024780)_g00150_f017652");
            Assert.True(output[0].LabelValues.Count == 24);
        }
    }
}
