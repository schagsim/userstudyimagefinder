﻿using System.Collections.Generic;
using System.Linq;
using UserStudyImageFinderRanker;
using Xunit;

namespace UserStudyImageFinderRankerUnitTests
{
    public class SumOfRelevantLabelsRankerTest
    {
        private const string integrationTestLabelsFolder = @".\RankerIntegrationTestsLabels";

        private List<ImageLabels> CreateImageLabelsFromIntegrationFolder()
        {
            return LabelParser.ReadAndParseImageLabels(integrationTestLabelsFolder);
        }

        private List<string> GenerateQueryParams()
        {
            return new List<string>()
            {
                "Photography"
            };
        }

        [Fact]
        public void RankerTestCorrectInput()
        {
            var ranker = new SumOfRelevantLabelsRanker(CreateImageLabelsFromIntegrationFolder());

            var rankerOutput = ranker.RankWithRelevancyOutput(3, GenerateQueryParams());

            Assert.True(rankerOutput.Count == 3);

            var rankerOutputList = rankerOutput.ToList();

            Assert.True(rankerOutputList[1].Key == "v00049_s00062(f017679-f018053)_g00135_f017875" && rankerOutputList[1].Value > 0.73799);
            Assert.True(rankerOutputList[2].Key == "v00054_s00017(f000694-f000777)_g00024_f000767" && rankerOutputList[2].Value > 0.67788);
        }
    }
}
