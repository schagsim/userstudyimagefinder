﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UserStudyImageFinderRanker
{
    public class SumOfRelevantLabelsRanker : IRanker
    {
        private List<ImageLabels> _imageLabels;

        public SumOfRelevantLabelsRanker(List<ImageLabels> imageLabels)
        {
            _imageLabels = imageLabels;
        }

        /// <summary>
        /// Outputs sum of all relevant query parameters.
        /// </summary>
        private double SumQueries(List<string> queryParams, ImageLabels imageLabels)
        {
            var labels = imageLabels.LabelValues.ToList();
            double sum = 0;

            for (int label = 0; label < labels.Count; label++)
            {
                if (queryParams.Contains(labels[label].Key)) sum += labels[label].Value;
            }

            return sum;
        }

        public List<string> RankWithoutRelevancyOutput(uint numberOfItemsInOutput, List<string> queryParams)
        {
            return new List<string>(RankWithRelevancyOutput(numberOfItemsInOutput, queryParams).Keys);
        }

        public Dictionary<string, double> RankWithRelevancyOutput(uint numberOfItemsInOutput, List<string> queryParams)
        {
            var outputList = new List<Tuple<string, double>>();

            foreach (var imageLabel in _imageLabels)
            {
                outputList.Add(new Tuple<string, double>(imageLabel.ImageId, SumQueries(queryParams, imageLabel)));
            }

            return outputList.OrderByDescending(x => x.Item2).Take((int)numberOfItemsInOutput).ToDictionary(x => x.Item1, x => x.Item2);
        }
    }
}
