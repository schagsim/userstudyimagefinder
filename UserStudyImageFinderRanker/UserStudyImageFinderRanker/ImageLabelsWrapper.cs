﻿using System.Collections.Generic;

namespace UserStudyImageFinderRanker
{
    public class ImageLabelsWrapper
    {
        public ImageLabelsWrapper(List<ImageLabels> loadedLabels)
        {
            LoadedImageLabels = loadedLabels;
        }
        public List<ImageLabels> LoadedImageLabels { get; private set; }
    }
}
