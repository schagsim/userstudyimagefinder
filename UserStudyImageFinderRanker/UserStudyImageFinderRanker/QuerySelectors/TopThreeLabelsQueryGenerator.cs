﻿using System.Collections.Generic;
using System.Linq;

namespace UserStudyImageFinderRanker
{
    public class TopThreeLabelsQueryGenerator : IQueryGenerator
    {
        public List<string> GenerateQueryFromLabels(ImageLabels imageLabels)
        {
            var sortedLabels = imageLabels.LabelValues.OrderByDescending(x => x.Value).ToList();

            var queriesWithLabels = sortedLabels.Take(3).ToList();

            var query = new List<string>();
            for (int label = 0; label < queriesWithLabels.Count; label++)
            {
                query.Add(queriesWithLabels[label].Key);
            }
            return query;
        }
    }
}
