﻿using System.Collections.Generic;

namespace UserStudyImageFinderRanker
{
    interface IQueryGenerator
    {
        List<string> GenerateQueryFromLabels(ImageLabels imageLabels);
    }
}
