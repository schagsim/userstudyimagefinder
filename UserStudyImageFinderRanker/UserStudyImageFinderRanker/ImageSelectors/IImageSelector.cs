﻿using System.Collections.Generic;

namespace UserStudyImageFinderRanker
{
    interface IImageSelector
    {
        /// <summary>
        /// Select image from image labels using a given metric depending on the specific image selector currently using.
        /// </summary>
        /// <param name="imageLabels">
        /// List of all images (with their IDs and their labels) from which to choose.
        /// </param>
        /// <returns>
        /// The position of the image ID selected.
        /// </returns>
        uint SelectImageId(List<ImageLabels> imageLabels);
    }
}
