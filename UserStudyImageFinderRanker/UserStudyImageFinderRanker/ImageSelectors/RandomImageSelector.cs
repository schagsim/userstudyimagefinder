﻿using System;
using System.Collections.Generic;

namespace UserStudyImageFinderRanker
{
    public class RandomImageSelector : IImageSelector
    {
        public uint SelectImageId(List<ImageLabels> imageLabels)
        {
            var randomGenerator = new Random((int)DateTime.Now.Ticks);

            return (uint)randomGenerator.Next(0, imageLabels.Count);
        }
    }
}
