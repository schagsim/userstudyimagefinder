﻿using System;
using System.Collections.Generic;
using UserStudyImageFinderRanker;

namespace ImageFinderUserStudyWeb
{
    public class UserSessions
    {
        public Dictionary<Guid, UserSessionInfo> GetUserSessions { get; private set; }

        private readonly ImageLabelsWrapper ImageLabelsWrapperHolder;

        public UserSessions(ImageLabelsWrapper imageLabelsWrapper)
        {
            GetUserSessions = new Dictionary<Guid, UserSessionInfo>();
            ImageLabelsWrapperHolder = imageLabelsWrapper;
        }

        public void AddNewUserSession(Guid userSessionId, ImageLabels imageLabels, int numberOfImagesToPresent, int numberOfColumnsInGallery)
        {
            var queryGenerator = new TopThreeLabelsQueryGenerator();
            var query = queryGenerator.GenerateQueryFromLabels(imageLabels);

            var ranker = new SumOfRelevantLabelsRanker(ImageLabelsWrapperHolder.LoadedImageLabels);
            var rankerOutput = ranker.RankWithRelevancyOutput((uint)numberOfImagesToPresent, query);

            var sorter = new RowsLabelSorter(rankerOutput, (uint)numberOfColumnsInGallery);
            var sorterOutput = sorter.Sort();

            var sessionInfo = new UserSessionInfo(userSessionId, imageLabels.ImageId, query, sorterOutput)
            {
                NumberOfColumnsPresented = numberOfColumnsInGallery,
                NumberOfImagesPresented = numberOfImagesToPresent
            };

            GetUserSessions.Add(userSessionId, sessionInfo);
        }
    }
}
