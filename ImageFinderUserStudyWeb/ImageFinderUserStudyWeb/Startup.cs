using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using UserStudyImageFinderRanker;

namespace ImageFinderUserStudyWeb
{
    public class Startup
    {
        private readonly string LabelFilesPath;

        private readonly string ThumbnailsFilesPath;

        private readonly string ImagesFolderFilePath;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            LabelFilesPath = Configuration["FilePaths:LabelFilesFolder"];
            ImagesFolderFilePath = Configuration["FilePaths:ImageFilesFolder"];
            ThumbnailsFilesPath = Configuration["FilePaths:ThumbnailFilesFolder"];
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            var parsedLabels = LabelParser.ReadAndParseImageLabels(LabelFilesPath);
            var imageLabelWrapper = new ImageLabelsWrapper(parsedLabels);            
            services.AddSingleton(imageLabelWrapper);

            services.AddSingleton(new UserSessions(imageLabelWrapper));

            int.TryParse(Configuration["GallerySettings:GalleryWidthPixels"], out var galleryWidth);
            int.TryParse(Configuration["GallerySettings:GalleryHeightPixels"], out var galleryHeight);
            int.TryParse(Configuration["GallerySettings:NumberOfColumns"], out var numberOfColumns);
            int.TryParse(Configuration["GallerySettings:NumberOfPicturesToPresent"], out var numberOfPicturesPresented);
            services.AddSingleton(new GlobalConfig(galleryWidth, galleryHeight, LabelFilesPath, ThumbnailsFilesPath, ImagesFolderFilePath, numberOfPicturesPresented, numberOfColumns));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();

            app.UseMvc();
        }
    }
}
