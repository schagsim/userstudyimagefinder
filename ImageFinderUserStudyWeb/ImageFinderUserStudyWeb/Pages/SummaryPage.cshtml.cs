﻿using System;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;

namespace ImageFinderUserStudyWeb.Pages
{
    public class SummaryPageModel : PageModel
    {
        private readonly UserSessions UserSessionsHolder;

        public UserSessionInfo UserSession { get; private set; }

        public SummaryPageModel(
            UserSessions userSessions)
        {
            UserSessionsHolder = userSessions;
        }

        public void OnGetSummaryAsync(Guid userSessionId)
        {
            UserSession = UserSessionsHolder.GetUserSessions[userSessionId];

            string filePath = "userSessions/" + UserSession.UserSessionId + ".json";

            System.IO.File.WriteAllText(filePath, JsonConvert.SerializeObject(UserSession));
        }
    }
}