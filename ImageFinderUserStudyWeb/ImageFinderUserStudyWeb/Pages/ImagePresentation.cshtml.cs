﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using UserStudyImageFinderRanker;

namespace ImageFinderUserStudyWeb.Pages
{
    public class ImagePresentationModel : PageModel
    {
        public string ImageId { get; private set; }

        public Guid UserSessionId { get; private set; }

        public ImageLabels UserImageLabels;

        private readonly ImageLabelsWrapper ImageLabelsHolder;
        private readonly UserSessions UserSessionsHolder;
        private readonly GlobalConfig GlobalConfigHolder;

        public ImagePresentationModel(
            ImageLabelsWrapper imageLabelsWrapper,
            UserSessions userSessions,
            GlobalConfig globalConfig)
        {
            ImageLabelsHolder = imageLabelsWrapper;
            UserSessionsHolder = userSessions;
            GlobalConfigHolder = globalConfig;

            UserSessionId = Guid.NewGuid();

            var imageSelector = new RandomImageSelector();
            var selectedImagePosition = imageSelector.SelectImageId(ImageLabelsHolder.LoadedImageLabels);

            UserImageLabels = ImageLabelsHolder.LoadedImageLabels[(int)selectedImagePosition];
            ImageId = UserImageLabels.ImageId;
        }

        public string ImagePath(string imageId)
        {
            return "http://herkules.ms.mff.cuni.cz/lineit/20k_images/images/" + $"{imageId}.jpg";
        }

        public async Task OnGetAsync()
        {
            await Task.Run(() => UserSessionsHolder.AddNewUserSession(
                UserSessionId, UserImageLabels,
                GlobalConfigHolder.NumberOfImagesToPresent, GlobalConfigHolder.NumberOfColumnsInGallery));
        }

        public IActionResult OnPostGallery(Guid userSessionId)
        {
            return RedirectToPage("ImageGallery", "Gallery", new { userSessionGuid = userSessionId });
        }
    }
}