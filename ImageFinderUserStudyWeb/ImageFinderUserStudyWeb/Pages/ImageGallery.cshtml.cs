﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ImageFinderUserStudyWeb.Pages
{
    public class ImageGalleryModel : PageModel
    {
        private readonly UserSessions UserSessionsHolder;

        private readonly GlobalConfig GlobalConfigHolder;

        public UserSessionInfo UserSession { get; private set; }

        public int NumberOfColumns { get; private set; }

        public int NumberOfRows { get; private set; }

        public int PictureWidthPixels { get; private set; }

        public int GalleryWidthPixels { get; private set; }

        public int GalleryHeightPixels { get; private set; }

        public ImageGalleryModel(
            UserSessions userSessionsInfo,
            GlobalConfig globalConfig
            )
        {
            UserSessionsHolder = userSessionsInfo;
            GlobalConfigHolder = globalConfig;
        }

        public string SizeInPixels(int size)
        {
            return $"{size}px";
        }

        public string ImagePath(string imageId)
        {
            return "http://herkules.ms.mff.cuni.cz/lineit/20k_images/images/" + $"{imageId}.jpg";
        }

        public void OnGetGallery(Guid userSessionGuid)
        {
            UserSession = UserSessionsHolder.GetUserSessions[userSessionGuid];

            NumberOfRows = UserSession.SortedResult.GetLength(0);
            NumberOfColumns = UserSession.SortedResult.GetLength(1);

            GalleryWidthPixels = GlobalConfigHolder.GalleryWidthPixels;
            GalleryHeightPixels = GlobalConfigHolder.GalleryHeightPixels;

            PictureWidthPixels = GalleryWidthPixels / NumberOfColumns;

            UserSession.UpdateGaleryDisplayTime(DateTime.Now);

            UserSession.NumberOfRowsPresented = NumberOfRows;
            UserSession.NumberOfColumnsPresented = NumberOfColumns;
            UserSession.GalleryPixelWidth = GalleryWidthPixels;
            UserSession.GalleryPixelHeight = GalleryHeightPixels;
        }

        public IActionResult OnPostImageClick(Guid userSessionGuid, string imageId)
        {
            UserSession = UserSessionsHolder.GetUserSessions[userSessionGuid];

            UserSession.FoundImageId = imageId;

            UserSession.TimeTakenToRespond = DateTime.Now - UserSession.TimeOfGalleryDisplay;

            UserSession.UserHasFoundImage(imageId);

            return RedirectToPage("SummaryPage", "Summary", new { userSessionId = userSessionGuid });
        }
    }
}