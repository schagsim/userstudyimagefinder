﻿namespace ImageFinderUserStudyWeb
{
    public class GlobalConfig
    {
        public int GalleryWidthPixels { get; private set; }

        public int GalleryHeightPixels { get; private set; }

        public string LabelsFolderFilePath { get; private set; }

        public string ThumbnailsFolderFilePath { get; private set; }

        public string ImagesFolderFilePath { get; private set; }

        public int NumberOfImagesToPresent { get; private set; }

        public int NumberOfColumnsInGallery { get; private set; }

        public GlobalConfig(
            int galleryWidthPixels,
            int galleryHeightPixels,
            string labelsFolderFilePath,
            string thumbnailsFolderFilePath,
            string imagesFolderFilePath,
            int numberOfImagesToPresent,
            int numberOfColumnsInGallery
            )
        {
            GalleryWidthPixels = galleryWidthPixels;
            GalleryHeightPixels = galleryHeightPixels;
            LabelsFolderFilePath = labelsFolderFilePath;
            ThumbnailsFolderFilePath = thumbnailsFolderFilePath;
            ImagesFolderFilePath = imagesFolderFilePath;
            NumberOfImagesToPresent = numberOfImagesToPresent;
            NumberOfColumnsInGallery = numberOfColumnsInGallery;
        }
    }
}
