﻿using System;
using System.Collections.Generic;

namespace ImageFinderUserStudyWeb
{
    public class UserSessionInfo
    {
        public Guid UserSessionId { get; private set; }

        public string PresentedImageId { get; private set; }

        public string FoundImageId { get; set; }

        public bool PresentedImageWasDisplayed { get; set; }

        public DateTime TimeOfGalleryDisplay { get; private set; }

        public TimeSpan TimeTakenToRespond { get; set; }

        public int NumberOfTimesScrolled { get; set; }

        public List<string> Query { get; private set; }

        public string[,] SortedResult { get; private set; }

        public int NumberOfImagesPresented { get; set; }

        public int NumberOfRowsPresented { get; set; }

        public int NumberOfColumnsPresented { get; set; }

        public int GalleryPixelWidth { get; set; }

        public int GalleryPixelHeight{ get; set; }

        private DateTime LastTimeScrolled { get; set; }

        public bool AnsweredCorrectly { get; set; }

        public int RowPositionPresented { get; set; }

        public UserSessionInfo(Guid userSessionId, string imageId, List<string> query, string[,] sortedResult)
        {
            UserSessionId = userSessionId;

            PresentedImageId = imageId;

            PresentedImageWasDisplayed = false;

            TimeOfGalleryDisplay = DateTime.Now;
           
            SortedResult = sortedResult;

            LastTimeScrolled = DateTime.Now;

            NumberOfTimesScrolled = 0;

            Query = query;

            AnsweredCorrectly = false;
        }

        public void UpdateGaleryDisplayTime(DateTime time)
        {
            TimeOfGalleryDisplay = time;
        }

        public void Scrolled(DateTime timeScrolled)
        {
            if ((timeScrolled - LastTimeScrolled).Milliseconds > 500)
            {
                NumberOfTimesScrolled++;
                LastTimeScrolled = timeScrolled;
            }
        }

        public void UserHasFoundImage(string imageId)
        {
            //TODO: Move this to async method when the gallery is presented.
            bool imageWasPresent = false;
            for (int i = 0; i < NumberOfRowsPresented; i++)
            {
                if (imageWasPresent) break;
                for (int j = 0; j < NumberOfColumnsPresented; j++)
                {
                    if (SortedResult[i, j] == PresentedImageId)
                    {
                        imageWasPresent = true;
                        break;
                    }
                }
            }

            PresentedImageWasDisplayed = imageWasPresent;

            if ((imageId == "NotPresent") && imageWasPresent)
            {
                AnsweredCorrectly = false;
            }
            else if ((imageId == "NotPresent") && !imageWasPresent)
            {
                AnsweredCorrectly = true;
            }
            else if ((imageId != "NotPresent") && imageWasPresent)
            {
                AnsweredCorrectly = PresentedImageId == FoundImageId;
            }
            else
            {
                AnsweredCorrectly = false;
            }
        }
    }
}
